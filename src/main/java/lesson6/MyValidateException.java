package main.java.lesson6;

public class MyValidateException extends Exception {

    public MyValidateException() {
    }

    public MyValidateException(String message) {
        super(message);
    }
}
