package main.java.lesson6;

public class Lesson_6 {
    public static void main(String[] args) {

//        try {
//            func1();
//        } catch (MyValidateException e) {
//            e.printStackTrace();
//        }

        try {
            func2();
        } catch (RuntimeException e) {
            e.printStackTrace();
        }
    }

    public static void func1() throws MyValidateException {
        try {
            Student stud = Student.deserialize(null);
        } catch (MyValidateException e) {
            System.out.println("Exp: " + e.getMessage());
            throw e;
        } catch (NullPointerException e) {
            System.out.println("Exp: " + e.getMessage());
            throw e;
        }
    }

    public static void func2() {
        try {
            Student stud = Student.deserialize(null);
        } catch (MyValidateException e) {
            System.out.println("Exp: " + e.getMessage());
            throw new RuntimeException(e);
        } catch (NullPointerException e) {
            System.out.println("Exp: " + e.getMessage());
            throw new RuntimeException(e);
        }
    }
}